from tkinter import *

def click(event):
    global scvalue
    text = event.widget.cget("text")
    if scvalue.get()== "Error"+text or scvalue.get()=='Error':
        if text.isdigit() :
            value=text
        else:
            value = ""
        scvalue.set(value)
        screen.update()

    elif text == "=":
        if scvalue.get().isdigit():
            value = int(scvalue.get())
        else:
            try:
                value = eval(screen.get())

            except Exception as e:
                print(e)
                value = "Error"


        scvalue.set(value)
        screen.update()

    elif text == "AC":
        scvalue.set("")
        screen.update()

    elif text== u"\u2190" :
        scvalue.set(scvalue.get()[:-1])
        screen.update()

    else:
        scvalue.set(scvalue.get() + text)
        screen.update()


root = Tk()
root.title("Calculator")

f = Frame(root, bg="light yellow")
scvalue = StringVar()
scvalue.set("")
screen = Entry(f, textvar=scvalue, font=("Courier New", 20,"bold"),bd=2)
screen.grid(row=0,column=0 , pady=5, padx=7,ipadx=5,ipady=5)
f.grid()

f2 = Frame(root,bg="light cyan")
b1 = Button(f2, text="1", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b1.bind('<Button-1>',click)
b1.grid(row=2,column=0)

b2 = Button(f2, text="2", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b2.bind('<Button-1>',click)
b2.grid(row=2,column=1)

b3 = Button(f2, text="3", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b3.bind('<Button-1>',click)
b3.grid(row=2,column=2)

b4 = Button(f2, text="4", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b4.bind('<Button-1>',click)
b4.grid(row=3,column=0)

b5 = Button(f2, text="5", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b5.bind('<Button-1>',click)
b5.grid(row=3,column=1)

b6 = Button(f2, text="6", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b6.bind("<Button-1>",click)
b6.grid(row=3,column=2)

b7 = Button(f2, text="7", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b7.bind("<Button-1>",click)
b7.grid(row=4,column=0)

b8 = Button(f2, text="8", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b8.bind("<Button-1>",click)
b8.grid(row=4,column=1)

b9 = Button(f2, text="9", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b9.bind('<Button-1>',click)
b9.grid(row=4,column=2)

b0 = Button(f2, text="0", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
b0.bind('<Button-1>',click)
b0.grid(row=5,column=1)

####################################################################################################

dot = Button(f2, text=".", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light cyan")
dot.bind("<Button-1>",click)
dot.grid(row=5,column=0)

equal = Button(f2, text="=", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light blue")
equal.bind("<Button-1>",click)
equal.grid(row=5,column=2)

mult = Button(f2, text="*", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light green")
mult.bind("<Button-1>",click)
mult.grid(row=2,column=3)

div = Button(f2, text="/", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light green")
div.bind("<Button-1>",click)
div.grid(row=3,column=3)

plus = Button(f2, text="+", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light green")
plus.bind("<Button-1>",click)
plus.grid(row=4,column=3)

minus = Button(f2, text="-", font=("Courier New", 20, "bold"), padx=30, pady=25, bd=4, bg="light green")
minus.bind("<Button-1>",click)
minus.grid(row=5,column=3)

AC = Button(f2, text="AC", font=("Courier New", 20, "bold"), padx=22, pady=14, bd=4, bg="light yellow")
AC.bind("<Button-1>",click)
AC.grid(row=1,column=3)

back = Button(f2, text=u"\u2190", font=("Courier New", 20, "bold"), padx=30, pady=14, bd=4, bg="light yellow")
back.bind("<Button-1>",click)
back.grid(row=1,column=2)

Open = Button(f2, text="(", font=("Courier New", 20, "bold"), padx=30, pady=14, bd=4, bg="light yellow")
Open.bind("<Button-1>",click)
Open.grid(row=1,column=0)

Close = Button(f2, text=")", font=("Courier New", 20, "bold"), padx=30, pady=14, bd=4, bg="light yellow")
Close.bind("<Button-1>",click)
Close.grid(row=1,column=1)

f2.grid(row=1,column=0)
root.mainloop()
