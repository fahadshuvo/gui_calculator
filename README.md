# Calculator
A simple GUI Calculator using Tkinter in Python

## About:
***Basic Calculator*** <br>
It has an entrybox that can accept input from the keyboard. Additionally, it also has some buttons which when pressed automatically inserts the corresponding character in the entrybox. After that, when the equal button is clicked, it takes the input from the entrybox and evaluates the result and displayed in entry box.
If any syntax error encountered in the entry box then it displayed an error messege.

***Tkinter Python*** <br>
Tkinter is a GUI framework for Python that allows for buttons, graphics and alot of other things to be done in a window
<br>

<p align="center">Calculator<br>
  <img src="images/a.png" width="300" height="450"><p/>

## Demo:

<p align="center" width="100%">
    <img width="30%" height="400" src="images/b.png">
    <img width="30%" height="400" src="images/c.png">
</p>
